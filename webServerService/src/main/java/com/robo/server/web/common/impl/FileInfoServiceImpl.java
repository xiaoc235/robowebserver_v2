package com.robo.server.web.common.impl;

import com.common.base.exception.BusinessException;
import com.robo.server.web.common.FileInfoService;
import com.robo.server.web.entity.File.FileInfoDto;
import com.robo.server.web.entity.File.FileUserMappingDto;
import com.robo.server.web.repository.common.FileInfoRepository;
import com.robo.server.web.repository.common.FileUserMappingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public class FileInfoServiceImpl implements FileInfoService {

    private static final Logger _logger = LoggerFactory.getLogger(FileInfoServiceImpl.class);

    @Autowired
    private FileInfoRepository fileInfoRepository;

    @Autowired
    private FileUserMappingRepository fileUserMappingRepository;

    /**
     * 保存文件信息
     * @param fileInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileInfoDto saveFileInfo(FileInfoDto fileInfoDto) throws BusinessException, DataAccessException, Exception{

        _logger.info("fileInfoDto==>"+fileInfoDto.toLogger());

        return fileInfoRepository.save(fileInfoDto);
    }

    /**
     * 根据Id，查询文件信息
     * @param fileId 文件Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileInfoDto getFileInfoById(final String fileId) throws BusinessException, DataAccessException, Exception{

        _logger.info("fileId=["+fileId+"]");

        return fileInfoRepository.findOne(fileId);
    }


    /**
     * 保存文件与用户的关联关系
     * @param fileUserMappingDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileUserMappingDto saveFileUserMapping(FileUserMappingDto fileUserMappingDto) throws BusinessException, DataAccessException, Exception{

        _logger.info("fileUserMappingDto==>"+fileUserMappingDto.toLogger());

        return fileUserMappingRepository.save(fileUserMappingDto);

    }

}
