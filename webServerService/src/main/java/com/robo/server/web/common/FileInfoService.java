package com.robo.server.web.common;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.File.FileInfoDto;
import com.robo.server.web.entity.File.FileUserMappingDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public interface FileInfoService {

    /**
     * 保存文件信息
     * @param fileInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileInfoDto saveFileInfo(FileInfoDto fileInfoDto) throws BusinessException, DataAccessException, Exception;

    /**
     * 根据Id，查询文件信息
     * @param fileId 文件Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileInfoDto getFileInfoById(final String fileId) throws BusinessException, DataAccessException, Exception;

    /**
     * 保存文件与用户的关联关系
     * @param fileUserMappingDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public FileUserMappingDto saveFileUserMapping(FileUserMappingDto fileUserMappingDto) throws BusinessException, DataAccessException, Exception;


}