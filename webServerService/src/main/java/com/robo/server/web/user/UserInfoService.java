package com.robo.server.web.user;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.user.UserBasicInfoDto;
import com.robo.server.web.entity.user.UserLoginInfoDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public interface UserInfoService {

    /**
     * 保存用户注册信息
     *
     * @param userLoginInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto saveUserLoginInfo(UserLoginInfoDto userLoginInfoDto) throws BusinessException, DataAccessException, Exception;

    /**
     * 根据用户Id，查询用户信息
     *
     * @param userId 用户Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto getUserLoginInfoByUserId(final String userId) throws BusinessException, DataAccessException, Exception;


    /**
     * 根据手机号码，查询用户信息
     *
     * @param mobile 手机号码
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserLoginInfoDto getUserLoginInfoByMobile(final String mobile) throws BusinessException, DataAccessException, Exception;

    /**
     * 根据用户ID，更新用户密码
     *
     * @param userId   用户Id
     * @param password 用户密码
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updatePasswordByUserId(final String userId, final String password) throws BusinessException, DataAccessException, Exception;

    public int updateMobileByUserId(final String userId, final String mobile) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存用户基本信息
     *
     * @param userBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserBasicInfoDto saveUserBasicInfo(UserBasicInfoDto userBasicInfoDto) throws BusinessException, DataAccessException, Exception;


    /**
     * 保存用户基本信息
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public UserBasicInfoDto getUserBasicInfoById(final String userId) throws BusinessException, DataAccessException, Exception;

}