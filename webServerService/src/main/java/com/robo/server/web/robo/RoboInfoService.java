package com.robo.server.web.robo;

import com.common.base.exception.BusinessException;
import com.common.base.model.MyPageResult;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import com.robo.server.web.entity.robo.RoboLoginInfoDto;
import com.robo.server.web.entity.robo.response.RoboInfoResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public interface RoboInfoService {

    /**
     * 保存机器人登录/登出信息
     * @param roboLoginInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboLoginInfoDto saveUserLoginInfo(RoboLoginInfoDto roboLoginInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据机器人Id，查询机器人登录/登出信息
     * @param roboId 机器人Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboLoginInfoDto getRoboLoginInfoByRoboId(final String roboId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据用户Id，查询所有的机器人登录/登出列表
     * @param userId 用户Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public MyPageResult<RoboInfoResponseDto> getRoboInfoPage(final String userId, final Pageable pageable) throws BusinessException,DataAccessException,Exception;

    /**
     * 更新/保存机器人基本信息
     * @param roboBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto saveUserBasicInfo(RoboBasicInfoDto roboBasicInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据机器人Id，查询机器人基本信息
     * @param roboId 机器人Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto getRoboBasicInfo(final String roboId) throws BusinessException,DataAccessException,Exception;

    public RoboBasicInfoDto getRoboBasicInfoNoStatus(final String roboId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据微信uin，查询机器人基本信息
     * @param userId 用户ID
     * @param uin 微信uin
     * @param status 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪  为空时查询所有
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public RoboBasicInfoDto getRoboBasicInfoByUin(final String userId,final String uin,final String status) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据用户Id,查询所有微信机器人信息(不包括取消追踪的机器人)
     * @param userId 用户ID
     * @param currentStatus 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9：取消追踪  可为空，为空时，查询所有有效机器人
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<RoboBasicInfoDto> getRoboBasicInfoList(final String userId,final String currentStatus) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据roboId，更新机器人当前状态
     * @param roboId 机器人id
     * @param currentStatus 当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateRoboCurrentStatusByRoboId(String roboId, String currentStatus, String memo) throws BusinessException,DataAccessException,Exception;


    /**
     * 获取在线机器人列表
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public List<RoboBasicInfoDto> getListByOnlie() throws DataAccessException, BusinessException, Exception;
}
