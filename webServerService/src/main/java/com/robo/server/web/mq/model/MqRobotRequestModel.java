package com.robo.server.web.mq.model;

import com.common.base.BaseDto;

import java.util.Map;

/**
 * Created by jianghaoming on 17/8/24.
 */
public class MqRobotRequestModel extends BaseDto {

    private String robotId; //机器人id

    private String uin; //微信uin

    private Integer command; //命令代码

    private Map<String,Object> data;


    public String getRobotId() {
        return robotId;
    }

    public void setRobotId(String robotId) {
        this.robotId = robotId;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public Integer getCommand() {
        return command;
    }

    public void setCommand(Integer command) {
        this.command = command;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
