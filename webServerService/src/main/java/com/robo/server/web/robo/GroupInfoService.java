package com.robo.server.web.robo;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;
import com.robo.server.web.entity.robo.group.GroupChatContentDto;
import com.robo.server.web.entity.robo.group.GroupPersonChangeDto;
import com.robo.server.web.entity.robo.group.GroupPersonInfoDto;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Service
public interface GroupInfoService {

    /**
     * 保存群组基本信息
     * @param groupBasicInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto saveGroupBasicInfo(GroupBasicInfoDto groupBasicInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组基本信息(批量)
     * @param groupBasicInfoDtoList
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupBasicInfoDto> saveGroupBasicInfo(List<GroupBasicInfoDto> groupBasicInfoDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 删除群组基本信息
     * @param userId 用户ID
     * @param uin 微信uin
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteGroupBasicInfoByUin(final String userId,final String uin) throws BusinessException,DataAccessException,Exception;

    /** 删除群组基本信息
     * @param id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public void deleteGroupBasicInfoById(final String id) throws BusinessException,DataAccessException,Exception;

    public void deleteGroupBasicInfoByUserId(final String userId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组ID，更新群组的授权状态
     * @param groupId 群组ID
     * @param authorizeStatus 授权状态 0：未授权 1：已授权
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public int updateGroupBasicInfoStatus(final String groupId,final String authorizeStatus) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组Id，查询群组基本信息
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto getGroupBasicInfoByGroupId(final String groupId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据userId和groupName，查询群组基本信息
     * @param userId 用户Id
     * @param groupName 群组名称
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupBasicInfoDto getGroupBasicInfoByUserIdAndName(final String userId,final String groupName) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据微信uin，查询群组基本信息列表
     * @param userId 用户ID
     * @param uin 微信uin
     * @param keyWord 查询条件，例如群组昵称，可为空
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupBasicInfoDto> getGroupBasicInfoPage(final String userId,final String uin,final String keyWord, final Pageable pageable) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据微信uin和授权状态，查询符合约束条件的群组列表（不分页）
     * @param userId 用户ID
     * @param uin 微信uin
     * @param keyWord 查询条件，例如群组昵称，可为空
     * @param authorizeStatus 授权状态 0：未授权 1：已授权；可为空，为空时，查询所有授权状态
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupBasicInfoDto> getAuthorizedGroupBasicInfoList(final String userId,final String uin,final String keyWord,final String authorizeStatus, final String isOwner) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组用户信息
     * @param groupPersonInfoDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonInfoDto saveGroupPersonInfo(GroupPersonInfoDto groupPersonInfoDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组用户信息(批量)
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupPersonInfoDto> saveGroupPersonInfo(List<GroupPersonInfoDto> groupPersonInfoDtoList) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组用户Id，查询群组用户信息
     * @param groupPersonId 群组用户Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonInfoDto getGroupPersonInfoByGroupPersonId(final String groupPersonId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组Id，查询群组成员列表（不分页）
     * @param groupId 群组Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public List<GroupPersonInfoDto> getGroupPersonInfoList(final String groupId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组Id，查询群组成员列表(分页)
     * @param groupId 群组Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupPersonInfoDto> getGroupPersonInfoPage(final String groupId,final Pageable pageable) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组id，删除群组成员信息
     * @param groupId
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteGroupPersonInfoByGroupId(final String groupId) throws DataAccessException,BusinessException,Exception;

    public void deleteGroupPersonInfoByUserId(final String userId) throws DataAccessException,BusinessException,Exception;

    /**
     * 根据群组id，删除群组成员信息
     * @param personId 群组成员ID
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public void deleteGroupPersonInfoByPersonId(final String personId) throws DataAccessException,BusinessException,Exception;

    /**
     * 保存群组聊天信息
     * @param groupChatContentDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupChatContentDto saveGroupChatContent(GroupChatContentDto groupChatContentDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据聊天Id，查询群组聊天信息
     * @param chatId 群组聊天Id
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupChatContentDto getGroupChatContentByChatId(final String chatId) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组Id，查询群组聊天信息列表
     * @param groupId 群组Id
     * @param pageable
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public Page<GroupChatContentDto> getGroupChatContentPage(final String groupId,final Pageable pageable) throws BusinessException,DataAccessException,Exception;

    /**
     * 保存群组成员变更信息
     * @param groupPersonChangeDto
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    public GroupPersonChangeDto saveGroupPersonChange(GroupPersonChangeDto groupPersonChangeDto) throws BusinessException,DataAccessException,Exception;

    /**
     * 根据群组名称和成员名称，查询群组成员信息列表
     * @param userId 用户Id
     * @param groupName 群组名称
     * @param personName 成员名称
     * @return
     * @throws BusinessException
     * @throws DataAccessException
     * @throws Exception
     */
    List<GroupPersonInfoDto> getGroupPersonInfoListByName(final String userId,final String groupName, final String personName) throws BusinessException,DataAccessException,Exception;

}
