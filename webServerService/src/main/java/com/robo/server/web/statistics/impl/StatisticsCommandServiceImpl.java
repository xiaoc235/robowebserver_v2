package com.robo.server.web.statistics.impl;

import com.common.base.CommConstants;
import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.statistics.StatisticsCommandDto;
import com.robo.server.web.repository.statistics.StatisticsCommandRepository;
import com.robo.server.web.statistics.StatisticsCommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @user jianghaoming 
 * @date 17/8/4  下午3:15
 *
 */
@Service
public class StatisticsCommandServiceImpl implements StatisticsCommandService {

    private static final Logger _logger = LoggerFactory.getLogger(StatisticsCommandServiceImpl.class);

    @Autowired
    private StatisticsCommandRepository statisticsCommandRepository;

    /**
     * 保存统计信息
     * @param dto
     * @return
     * @throws DataAccessException
     * @throws BusinessException
     * @throws Exception
     */
    public StatisticsCommandDto save(StatisticsCommandDto dto) throws DataAccessException,BusinessException,Exception{
        if(null == dto){
            throw new BusinessException(CommConstants.OBJECT_NOT_NUL);
        }
        _logger.info("save dto.log ==>"+dto.toLogger());
        return statisticsCommandRepository.save(dto);
    }

}
