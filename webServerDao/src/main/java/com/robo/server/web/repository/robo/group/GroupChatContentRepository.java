package com.robo.server.web.repository.robo.group;

import com.robo.server.web.entity.robo.group.GroupChatContentDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_group_chat_content")
public interface GroupChatContentRepository extends JpaRepository<GroupChatContentDto,String> {
}
