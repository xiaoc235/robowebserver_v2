package com.robo.server.web.em.cache;


/**
 * Created by jianghaoming on 17/3/7.
 */
public enum CacheRobotImgEnum {

    //机器人图片缓存

    HEADIMG("head_img_");


    private  String  value ;

    private CacheRobotImgEnum(final String  _value) {
        this.value = _value;
    }

    public String getValue() {
        return value;
    }
}
