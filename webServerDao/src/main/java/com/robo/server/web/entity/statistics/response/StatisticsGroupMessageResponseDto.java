package com.robo.server.web.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 群组消息的统计
 * Created by conor on 2017/8/29.
 */
public class StatisticsGroupMessageResponseDto extends BaseDto {

    private String groupId;//群组Id
    private String messageType;//消息类型 0：文本 1：表情 2：文章 3：图片 4：红包 5：视频 6：语音 7：其他
    private Integer messageCount;//当前消息类型的消息数量

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

}



