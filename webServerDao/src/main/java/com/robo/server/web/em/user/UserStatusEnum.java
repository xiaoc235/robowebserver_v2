package com.robo.server.web.em.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 0:启用，1停用
 */
public enum UserStatusEnum {
    NORMAL("0"),
    FROST("1");

    // 定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {

    }

    // 构造函数，枚举类型只能为私有
    private UserStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }
    public static String getValue(UserStatusEnum enums) {
        return enums.value;
    }

    public static List<UserStatusEnum> getAllList() {
        UserStatusEnum[] types = UserStatusEnum.values();
        List<UserStatusEnum> result = new ArrayList<UserStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return desc;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
