package com.robo.server.web.repository.common;

import com.robo.server.web.entity.File.FileUserMappingDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/7/19.
 */
@Repository
@Table(name="t_file_user_mapping")
public interface FileUserMappingRepository extends JpaRepository<FileUserMappingDto,String> {
}
