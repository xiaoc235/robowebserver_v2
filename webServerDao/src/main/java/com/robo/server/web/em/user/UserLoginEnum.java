package com.robo.server.web.em.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 0 手机密码登录，1 token登录
 */
public enum UserLoginEnum {
    MOBILE("0"),
    TOKEN("1");


    // 定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();

    // 构造函数，枚举类型只能为私有
    private UserLoginEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }
    public static String getValue(UserLoginEnum enums) {
        return enums.value;
    }

    public static List<UserLoginEnum> getAllList() {
        UserLoginEnum[] types = UserLoginEnum.values();
        List<UserLoginEnum> result = new ArrayList<UserLoginEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return desc;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
