package com.robo.server.web.em.cache;


/**
 * websocket 相关缓存key
 */
public enum CacheWebScoketEnum {

    //用户相关的缓存key
    wechatUUid("wechatUUid_"),

    roboId("roboId_"),

    reboLogin_message_("robo_login_"), //登录消息

    //客户端list
    client_lsit("clientList");


    private  String  value ;

    private CacheWebScoketEnum(final String  _value) {
        this.value = _value;
    }

    public String getValue() {
        return value;
    }
}
