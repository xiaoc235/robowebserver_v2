package com.robo.server.web.message;

/**
 * Created by jianghaoming on 17/3/6.
 */
public class RoboMessage {


    public static final String chat_dic_limit_count = "自动回复设置暂只支持20条";

    public static final String ROBO_ID = "机器人ID不能为空";

    public static final String UIN = "uin不能为空";

    public static final String ID = "id不能为空";

    public static final String GROUP_ID = "群组Id不能为空";

    public static final String GROUP_PERSON_ID = "群组用户Id不能为空";

    public static final String CHAT_ID = "群组聊天Id不能为空";

    public static final String SETTING_ID = "机器人功能开关类型不能为空";

    public static final String CHAT_DICT_ID = "群组聊天匹配Id不能为空";

    public static final String GROUP_NOT_AUTHORIZATION = "该群组没有授权功能";

    public static final String SWITCH_TYPE_NOT_EXIST = "机器人功能开关类型不存在";
    public static final String SETTING_TYPE_NOT_EXIST = "机器人功能开关类型不存在";
    public static final String SETTING_CONTENT_TYPE_NOT_EXIST = "机器人提示语设置状态不存在";
    public static final String ROBO_CURRENT_STATUS_NOT_EXIST = "机器人当前状态不存在";
    public static final String authorizeStatus_not_exist = "群组授权状态不存在";
    public static final String GROUP_PERSON_CHANE_TYPE_NOT_EXIST = "群组成员变更类型不存在";

    public static abstract class message{
        public static final String login = "机器人正常登录";
        public static final String loginout = "机器人正常登出";
        public static final String status_not_exist = "机器人状态参数设置有误";
    }

    public static final String start_time = "起始时间不能为空";
    public static final String end_time = "截止时间不能为空";
    public static final String current_day = "当前日期不能为空";

}
