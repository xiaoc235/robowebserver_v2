package com.robo.server.web.repository.robo.group;

import com.robo.server.web.entity.robo.group.GroupPersonChangeDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/8/24.
 */
@Repository
@Table(name = "t_group_person_change")
public interface GroupPersonChangeRepository extends JpaRepository<GroupPersonChangeDto,String> {

}
