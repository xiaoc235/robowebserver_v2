package com.robo.server.web.entity.robo.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/23.
 */
public class GroupPersonResponseDto extends BaseDto {


    private String groupPersonId;//群组成员ID
    private String personNickName;//群员昵称

    public String getGroupPersonId() {
        return groupPersonId;
    }

    public void setGroupPersonId(String groupPersonId) {
        this.groupPersonId = groupPersonId;
    }

    public String getPersonNickName() {
        return personNickName;
    }

    public void setPersonNickName(String personNickName) {
        this.personNickName = personNickName;
    }
}
