package com.robo.server.web.entity.File;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/3/14.
 */
@Entity
@Table(name="t_file_user_mapping")
public class FileUserMappingDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    private String fileId;//文件Id

    private String userId;//用户Id

    private String fileType;//文件类型 0：配置文件

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
