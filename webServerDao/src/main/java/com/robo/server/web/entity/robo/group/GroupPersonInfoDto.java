package com.robo.server.web.entity.robo.group;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/7/12.
 */
@Entity
@Table(name="t_group_person_info")
public class GroupPersonInfoDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String groupPersonId;//群组成员ID
    private String groupId;//群组ID
    private String ownerUin;
    private String pyInitial;
    private String pyQuanPin;//拼音全拼
    private String province;//省份
    private String city;//城市

    private String groupName; //群组名称

    private String personNickName;//群员昵称

    private String userId; //用户id

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPersonNickName() {
        return personNickName;
    }

    public void setPersonNickName(String personNickName) {
        this.personNickName = personNickName;
    }

    public String getGroupPersonId() {
        return groupPersonId;
    }

    public void setGroupPersonId(String groupPersonId) {
        this.groupPersonId = groupPersonId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getOwnerUin() {
        return ownerUin;
    }

    public void setOwnerUin(String ownerUin) {
        this.ownerUin = ownerUin;
    }

    public String getPyInitial() {
        return pyInitial;
    }

    public void setPyInitial(String pyInitial) {
        this.pyInitial = pyInitial;
    }

    public String getPyQuanPin() {
        return pyQuanPin;
    }

    public void setPyQuanPin(String pyQuanPin) {
        this.pyQuanPin = pyQuanPin;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
