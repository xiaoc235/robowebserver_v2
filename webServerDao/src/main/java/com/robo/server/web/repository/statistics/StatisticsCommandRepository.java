package com.robo.server.web.repository.statistics;

import com.robo.server.web.entity.statistics.StatisticsCommandDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;


@Repository
@Table(name = "t_statistics_command")
public interface StatisticsCommandRepository extends JpaRepository<StatisticsCommandDto,Integer> {


}
