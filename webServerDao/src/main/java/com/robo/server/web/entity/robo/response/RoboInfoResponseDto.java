package com.robo.server.web.entity.robo.response;

import com.common.base.BaseDto;
import com.robo.server.web.entity.robo.group.GroupBasicInfoDto;

import java.util.List;

/**
 * Created by conor on 2017/8/1.
 */
public class RoboInfoResponseDto extends BaseDto {

    private String roboId;//机器人Id
    private String uin;
    private String nickName;//昵称
    private String currentStatus;//当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪

    private List<GroupBasicInfoDto> groupList;//微信群组列表信息


    public String getRoboId() {
        return roboId;
    }

    public void setRoboId(String roboId) {
        this.roboId = roboId;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<GroupBasicInfoDto> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<GroupBasicInfoDto> groupList) {
        this.groupList = groupList;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
