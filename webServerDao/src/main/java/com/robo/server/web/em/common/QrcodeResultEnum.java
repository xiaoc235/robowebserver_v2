package com.robo.server.web.em.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 扫描结果 0：成功 1：失败
 */
public enum QrcodeResultEnum {
    SUCCESS("0"),
    FAILURE("1"),
    ERROR("2");

    //定义私有变量
    private  String  value ;

    //构造函数，枚举类型只能为私有
    private QrcodeResultEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(SUCCESS.getValue(), "扫描成功");
        desc.put(FAILURE.getValue(), "扫描失败");
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }

}
