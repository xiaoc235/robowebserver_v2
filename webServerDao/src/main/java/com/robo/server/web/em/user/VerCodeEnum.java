package com.robo.server.web.em.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 0:注册，1:忘记密码
 */
public enum VerCodeEnum {
    REGISTER("0"),
    FORGET_PASSWORD("1");


    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(REGISTER.getValue(),"手机注册");
        desc.put(FORGET_PASSWORD.getValue(),"忘记密码");
    }

    //构造函数，枚举类型只能为私有
    private VerCodeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }
    public static String getValue(VerCodeEnum enums) {
        return enums.value;
    }

    public static List<VerCodeEnum> getAllList() {
        VerCodeEnum[] types = VerCodeEnum.values();
        List<VerCodeEnum> result = new ArrayList<VerCodeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return desc;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
