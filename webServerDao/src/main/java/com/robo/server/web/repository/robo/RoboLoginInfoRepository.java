package com.robo.server.web.repository.robo;

import com.robo.server.web.entity.robo.RoboLoginInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_robo_login_info")
public interface RoboLoginInfoRepository extends JpaRepository<RoboLoginInfoDto,String> {
}
