package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 设备类型 0:winclient 1:pyclient 2:app
 */
public enum RoboDeviceTypeEnum {
    winclient("0"),
    pyclient("1"),
    app("2");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(winclient.getValue(),"client端");
        desc.put(pyclient.getValue(),"pc端");
        desc.put(app.getValue(),"app");
    }

    //构造函数，枚举类型只能为私有
    private RoboDeviceTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboDeviceTypeEnum enums) {
        return enums.value;
    }

    public static List<RoboDeviceTypeEnum> getAllList() {
        RoboDeviceTypeEnum[] types = RoboDeviceTypeEnum.values();
        List<RoboDeviceTypeEnum> result = new ArrayList<RoboDeviceTypeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
