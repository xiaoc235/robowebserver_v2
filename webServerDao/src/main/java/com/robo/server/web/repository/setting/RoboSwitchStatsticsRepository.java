package com.robo.server.web.repository.setting;

import com.robo.server.web.entity.setting.RoboSwitchStatsticsDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/8/15.
 */
@Repository
@Table(name = "t_robo_switch_statstics")
public interface RoboSwitchStatsticsRepository extends JpaRepository<RoboSwitchStatsticsDto,String> {
}
