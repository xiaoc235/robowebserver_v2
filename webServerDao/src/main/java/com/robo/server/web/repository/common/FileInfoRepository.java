package com.robo.server.web.repository.common;

import com.robo.server.web.entity.File.FileInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

/**
 * Created by conor on 2017/7/19.
 */
@Repository
@Table(name="t_file_info")
public interface FileInfoRepository extends JpaRepository<FileInfoDto,String> {
}
