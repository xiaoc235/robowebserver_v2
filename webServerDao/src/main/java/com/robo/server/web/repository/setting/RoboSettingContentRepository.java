package com.robo.server.web.repository.setting;

import com.robo.server.web.entity.setting.RoboSettingContentDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;

/**
 * Created by conor on 2017/8/15.
 */
@Repository
@Table(name = "t_robo_setting_content")
public interface RoboSettingContentRepository extends JpaRepository<RoboSettingContentDto,String> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update RoboSettingContentDto t set t.status=:status where t.id=:id ")
    int updateRoboSettingContentStatusById(@Param("id") String id,@Param("status") String status);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update RoboSettingContentDto t set t.status=:status where t.userId=:userId and t.uin=:uin ")
    int updateAllRoboSettingContentStatus(@Param("userId") String userId,@Param("uin") String uin,@Param("status") String status);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update RoboSettingContentDto t set t.content=:content where t.id=:id ")
    int updateRoboSettingContentById(@Param("id") String id,@Param("content") String content);
}
