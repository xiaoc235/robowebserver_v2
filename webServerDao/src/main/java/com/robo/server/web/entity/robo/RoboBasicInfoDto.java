package com.robo.server.web.entity.robo;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/7/12.
 */
@Entity
@Table(name="t_robo_basic_info")
public class RoboBasicInfoDto extends BaseTimeDto{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String userId;//用户Id
    private String roboId;//机器人Id
    private String uin;
    private String nickName;//昵称
    private String pyInitial;
    private String remarkName;
    private String remarkPyInitial;
    private String province;//省份
    private String city;//城市
    private String sex;//性别 0：男 1：女
    private String signature;//签名
    private String snsFlag;
    private String currentStatus;//当前状态 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪

    private String headImg; //机器人头像

    private String pid; //python 进程id


    private String loginMemo;//登录异常信息

    public String getLoginMemo() {
        return loginMemo;
    }

    public void setLoginMemo(String loginMemo) {
        this.loginMemo = loginMemo;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoboId() {
        return roboId;
    }

    public void setRoboId(String roboId) {
        this.roboId = roboId;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPyInitial() {
        return pyInitial;
    }

    public void setPyInitial(String pyInitial) {
        this.pyInitial = pyInitial;
    }

    public String getRemarkName() {
        return remarkName;
    }

    public void setRemarkName(String remarkName) {
        this.remarkName = remarkName;
    }

    public String getRemarkPyInitial() {
        return remarkPyInitial;
    }

    public void setRemarkPyInitial(String remarkPyInitial) {
        this.remarkPyInitial = remarkPyInitial;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSnsFlag() {
        return snsFlag;
    }

    public void setSnsFlag(String snsFlag) {
        this.snsFlag = snsFlag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
