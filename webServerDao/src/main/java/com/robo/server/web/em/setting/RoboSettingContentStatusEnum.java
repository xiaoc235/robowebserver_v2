package com.robo.server.web.em.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 状态 0：待启用 1：启用
 * Created by conor on 2017/8/16.
 */
public enum RoboSettingContentStatusEnum {

    CLOSE("0"),
    OPEN("1");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(CLOSE.getValue(),"待启用");
        desc.put(OPEN.getValue(),"启用");
    }

    //构造函数，枚举类型只能为私有
    private RoboSettingContentStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboSettingContentStatusEnum enums) {
        return enums.value;
    }

    public static List<RoboSettingContentStatusEnum> getAllList() {
        RoboSettingContentStatusEnum[] types = RoboSettingContentStatusEnum.values();
        List<RoboSettingContentStatusEnum> result = new ArrayList<RoboSettingContentStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
