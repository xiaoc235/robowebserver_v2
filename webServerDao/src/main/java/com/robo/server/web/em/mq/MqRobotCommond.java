package com.robo.server.web.em.mq;

/**
 * Created by yoki on 6/28/17.
 */
public enum MqRobotCommond {
    RELOAD(1),
    START (2),
    QUIT(4),
    BINGDING(3),
    UPDATE_CONFIG(5),
    TIREN(6),
    REFRESH_GROUP(7);

     int constant;
     private MqRobotCommond(int i) {
         constant = i;
     }

    public int val() {
        return this.constant;
    }


}
