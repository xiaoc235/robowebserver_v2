package com.robo.server.web.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 群组统计
 * Created by conor on 2017/8/29.
 */
public class StatisticsGroupInfoResponseDto extends BaseDto {

    private String groupId;//群组Id
    private Integer personTotal;//当前群组总成员数
    private Integer joinCount;//当前群组新增成员数
    private Integer leaveCount;//当前群组离开成员数
    private Integer messageTotal;//消息总数量

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getPersonTotal() {
        return personTotal;
    }

    public void setPersonTotal(Integer personTotal) {
        this.personTotal = personTotal;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(Integer joinCount) {
        this.joinCount = joinCount;
    }

    public Integer getLeaveCount() {
        return leaveCount;
    }

    public void setLeaveCount(Integer leaveCount) {
        this.leaveCount = leaveCount;
    }

    public Integer getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(Integer messageTotal) {
        this.messageTotal = messageTotal;
    }

}



