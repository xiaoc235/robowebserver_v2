package com.robo.server.web;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by jianghaoming on 2017/4/230:10.
 */
@EntityScan(basePackages="com.robo.server.web.entity")
@EnableJpaRepositories(basePackages={"com.robo.server.web.repository"})
@Configuration
public class JPAConfig {

}
