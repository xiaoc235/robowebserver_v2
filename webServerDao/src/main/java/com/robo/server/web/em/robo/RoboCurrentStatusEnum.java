package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 0: 未登录 1: 正常登录中 2：正常退出 3：异常退出 9:取消追踪
 */
public enum RoboCurrentStatusEnum {
    login_no("0"),
    login_ing("1"),
    loginout("2"),
    loginout_exception("3"),
    login_cancel("9");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(login_no.getValue(),"未登录");
        desc.put(login_ing.getValue(),"正常登录中");
        desc.put(loginout.getValue(),"正常退出");
        desc.put(loginout_exception.getValue(),"异常退出");
        desc.put(login_cancel.getValue(),"取消追踪");
    }

    //构造函数，枚举类型只能为私有
    private RoboCurrentStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboCurrentStatusEnum enums) {
        return enums.value;
    }

    public static List<RoboCurrentStatusEnum> getAllList() {
        RoboCurrentStatusEnum[] types = RoboCurrentStatusEnum.values();
        List<RoboCurrentStatusEnum> result = new ArrayList<RoboCurrentStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
