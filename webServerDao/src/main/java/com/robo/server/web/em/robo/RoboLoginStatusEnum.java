package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登出状态 0: login, 1: logout
 */
public enum RoboLoginStatusEnum {
    login("0"),
    loginout("1");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(login.getValue(),"登录");
        desc.put(loginout.getValue(),"登出");
    }

    //构造函数，枚举类型只能为私有
    private RoboLoginStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboLoginStatusEnum enums) {
        return enums.value;
    }

    public static List<RoboLoginStatusEnum> getAllList() {
        RoboLoginStatusEnum[] types = RoboLoginStatusEnum.values();
        List<RoboLoginStatusEnum> result = new ArrayList<RoboLoginStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
