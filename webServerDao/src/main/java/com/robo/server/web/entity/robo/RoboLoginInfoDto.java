package com.robo.server.web.entity.robo;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/7/12.
 */
@Entity
@Table(name="t_robo_login_info")
public class RoboLoginInfoDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String roboId;//机器人Id
    private String userId;//用户Id
    private String loginStatus;//登出状态 0: login, 1: logout
    private String deviceType;//设备类型 0:winclient  1:pyclient  2:app

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoboId() {
        return roboId;
    }

    public void setRoboId(String roboId) {
        this.roboId = roboId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
