package com.robo.server.web.repository.robo;

import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_robo_basic_info")
public interface RoboBasicInfoRepository extends JpaRepository<RoboBasicInfoDto,String> {

    @Query("select t from RoboBasicInfoDto t where t.currentStatus != '9' and t.userId=:userId order by t.updateTime asc")
    List<RoboBasicInfoDto> getRoboBasicInfoList(@Param("userId") String userId);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update RoboBasicInfoDto t set t.currentStatus=:currentStatus , login_memo = :memo where t.roboId=:roboId")
    int updateRoboCurrentStatusByRoboId(@Param("roboId") String roboId, @Param("memo") String memo ,@Param("currentStatus") String currentStatus);

    @Query(value = "select * from t_robo_basic_info where current_status = '1' and pid is not null", nativeQuery = true)
    List<RoboBasicInfoDto> getPidListByStatus();
}
