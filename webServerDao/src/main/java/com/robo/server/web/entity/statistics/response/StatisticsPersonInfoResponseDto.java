package com.robo.server.web.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 群组成员统计
 * Created by conor on 2017/8/29.
 */
public class StatisticsPersonInfoResponseDto extends BaseDto {

    private String personId;//群组成员Id
    private String groupId;//群组Id
    private Integer messageTotal;//消息总数量
    private String currentDay;//当前日期

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(Integer messageTotal) {
        this.messageTotal = messageTotal;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }

}



