package com.robo.server.web.dao.statistics;

import com.robo.server.web.entity.statistics.response.StatisticsGroupInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupMessageResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonMessageResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by conor on 2017/8/30.
 */
@Component
public interface StatisticsRoboDao {

    /**
     * 查询所有群组成员统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsPersonInfoResponseDto> getStatisticsPersonInfoResponseList(final String currentDay) throws DataAccessException;

    /**
     * 查询所有群组成员消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsPersonMessageResponseDto> getStatisticsPersonMessageResponseList(final String currentDay) throws DataAccessException;

    /**
     * 查询所有群组统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupInfoResponseDto> getStatisticsGroupInfoResponseList(final String currentDay) throws DataAccessException;

    /**
     * 查询所有群组消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupMessageResponseDto> getStatisticsGroupMessageResponseList(final String currentDay) throws DataAccessException;

}
