package com.robo.server.web.repository.robo.group;

import com.robo.server.web.entity.robo.group.GroupPersonInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import javax.transaction.Transactional;

/**
 * Created by conor on 2017/7/12.
 */
@Repository
@Table(name = "t_group_person_info")
public interface GroupPersonInfoRepository extends JpaRepository<GroupPersonInfoDto,String> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from GroupPersonInfoDto t where t.groupId=:groupId")
    void deleteGroupPersonInfoByGroupId(@Param("groupId") String groupId);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from GroupPersonInfoDto t where t.userId=:userId")
    void deleteGroupPersonInfoByUserId(@Param("userId") String userId);
}
