package com.robo.server.web.dao.statistics.impl;

import com.robo.server.web.dao.BaseDaoImpl;
import com.robo.server.web.dao.statistics.StatisticsRoboDao;
import com.robo.server.web.entity.statistics.response.StatisticsGroupInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsGroupMessageResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonInfoResponseDto;
import com.robo.server.web.entity.statistics.response.StatisticsPersonMessageResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by conor on 2017/8/30.
 */
@Component
public class StatisticsRoboDaoImpl extends BaseDaoImpl implements StatisticsRoboDao {

    /**
     * 查询所有群组成员统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsPersonInfoResponseDto> getStatisticsPersonInfoResponseList(final String currentDay) throws DataAccessException{

        final String sql = "select person.group_id,person.group_person_id as person_id,chat.message_total from t_group_person_info person " +
                "left join(select group_id,person_nick_name,count(chat_id) as message_total from t_group_chat_content " +
                "where create_time like CONCAT('%',:currentDay,'%') GROUP BY group_id,person_nick_name) chat " +
                "on chat.group_id=person.group_id and person.person_nick_name=chat.person_nick_name";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("currentDay",currentDay);

        return queryListEntity(sql,params,StatisticsPersonInfoResponseDto.class);

    }

    /**
     * 查询所有群组成员消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsPersonMessageResponseDto> getStatisticsPersonMessageResponseList(final String currentDay) throws DataAccessException{

        final String sql = "select person.group_id,person.group_person_id as person_id,chat.message_type,chat.message_count from t_group_person_info person " +
                "left join (select group_id,person_nick_name,message_type, count(chat_id) as message_count,create_time from t_group_chat_content " +
                "GROUP BY group_id, person_nick_name,message_type,DATE_FORMAT(create_time,'%Y-%m-%d') HAVING group_id is not null and group_id != '' and create_time like CONCAT('%',:currentDay,'%')) chat " +
                "on chat.group_id = person.group_id and chat.person_nick_name = person.person_nick_name";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("currentDay",currentDay);

        return queryListEntity(sql,params,StatisticsPersonMessageResponseDto.class);
    }

    /**
     * 查询所有群组统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupInfoResponseDto> getStatisticsGroupInfoResponseList(final String currentDay) throws DataAccessException{

        final String sql = "select basic.group_id,person.person_total,ch.join_count,ch.leave_count,chat.message_total from t_group_basic_info basic " +
                "left join (select group_id,count(group_person_id) as person_total from t_group_person_info GROUP BY group_id) person on person.group_id=basic.group_id " +
                "left join (select group_id,sum(case when (type='10' or type='11') then 1 else 0 end) as join_count,sum(case when (type='20' or type='21') then 1 else 0 end) as leave_count from t_group_person_change " +
                "where create_time like CONCAT('%',:currentDay1,'%') GROUP BY group_id) ch on ch.group_id=basic.group_id " +
                "left join(select group_id,count(chat_id) as message_total from t_group_chat_content where create_time like CONCAT('%',:currentDay2,'%') GROUP BY group_id) chat on chat.group_id=basic.group_id";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("currentDay1",currentDay);
        params.put("currentDay2",currentDay);

        return queryListEntity(sql,params,StatisticsGroupInfoResponseDto.class);
    }

    /**
     * 查询所有群组消息统计信息
     * @param currentDay 当前日期（yyyy-MM-dd）
     * @return
     * @throws DataAccessException
     */
    public List<StatisticsGroupMessageResponseDto> getStatisticsGroupMessageResponseList(final String currentDay) throws DataAccessException{

        final String sql = "select basic.group_id,chat.message_type,chat.message_count from t_group_basic_info basic " +
                "left join (select group_id,message_type,count(chat_id) as message_count,create_time from t_group_chat_content " +
                "GROUP BY group_id,message_type,DATE_FORMAT(create_time,'%Y-%m-%d') HAVING group_id is not null and group_id !='' and create_time like CONCAT('%',:currentDay,'%')) chat on chat.group_id = basic.group_id ";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("currentDay",currentDay);

        return queryListEntity(sql,params,StatisticsGroupMessageResponseDto.class);
    }

}
