package com.robo.server.web.entity.setting.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/15.
 */
public class RoboSettingContentResponseDto extends BaseDto {

    private String id;
    private String type;//类型 0：踢人 1：欢迎 2：规定 98:切换 99：图灵机器人Key
    private String content;//内容
    private String status;//状态 0：待启用 1：启用

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
