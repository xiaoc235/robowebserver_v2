package com.robo.server.web.entity.statistics;

import com.robo.server.web.entity.BaseTimeDto;

import javax.persistence.*;

/**
 * Created by jianghaoming on 17/8/4.
 */
@Entity
@Table(name = "t_statistics_command")
public class StatisticsCommandDto extends BaseTimeDto{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String userId;

    private String command;

    private String userName;

    private String groupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
