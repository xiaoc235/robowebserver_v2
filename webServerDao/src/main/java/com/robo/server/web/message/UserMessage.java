package com.robo.server.web.message;

/**
 * Created by jianghaoming on 17/3/6.
 */
public class UserMessage {

    public static final String VERCODE_FAIL = "验证码错误";
    public static final String VERCODE_TIMOUT = "验证码已失效";
    public static final String VERCODE_SUCC = "验证通过";

    public static final String NOT_BINGDING_MOBILE = "未绑定手机号码";

    public static final String USER_NULL = "未找到相关用户信息";

    public static abstract class FieldVALID{
        public static final String MOBILE = "手机号码不能为空";
        public static final String MOBILE_VALID = "请输入正确的手机号码";
        public static final String PASSWORD = "密码不能为空";
        public static final String USERID = "用户编号不能为空";
    }


    public static abstract class LoginMessage {
        public static final String LOGIN_FAIL = "用户名或密码错误";
        public static final String LOGIN_SUCC = "登录成功";
        public static final String LOGIN_OUT = "安全退出成功";
        public static final String NOT_REGISTER = "你还未进行注册，请先注册";
        public static final String STATUS_FROST = "你的账户已被冻结";
    }

    public static abstract class RegisterMessage {
        public static final String MOBILE_ALREADY_EXIST = "该手机号码已经存在";
        public static final String MOBILE_NOT_EXIST = "请先进行注册";
        public static final String RESIGER_SUCC = "注册成功";
        public static final String RESIGER_FAIL = "注册失败";
    }

    public static abstract class PwdMessage {
        public static final String PWD_UNMATCHED = "原密码输入错误";
        public static final String PWD_SAME = "新密码不能和原密码一样";
        public static final String PWD_EXIST = "密码已经存在，不能重新设置";
    }



}
