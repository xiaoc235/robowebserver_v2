package com.robo.server.web.em.robo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 授权状态 0：未授权 1：已授权
 */
public enum GroupAuthorizeStatusEnum {
    NO("0"),
    YES("1");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(YES.getValue(),"已授权");
        desc.put(NO.getValue(),"未授权");
    }

    //构造函数，枚举类型只能为私有
    private GroupAuthorizeStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(GroupAuthorizeStatusEnum enums) {
        return enums.value;
    }

    public static List<GroupAuthorizeStatusEnum> getAllList() {
        GroupAuthorizeStatusEnum[] types = GroupAuthorizeStatusEnum.values();
        List<GroupAuthorizeStatusEnum> result = new ArrayList<GroupAuthorizeStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
