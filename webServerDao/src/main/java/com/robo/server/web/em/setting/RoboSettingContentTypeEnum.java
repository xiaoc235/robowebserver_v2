package com.robo.server.web.em.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类型 0：踢人 1：欢迎 2：规定  98：切换（人工与机器人回复切换） 99：图灵机器人Key
 */
public enum RoboSettingContentTypeEnum {
    tiRen("0"),
    welcome("1"),
    guiDing("2"),
    qieHuan("98"),
    tulingKey("99");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(tiRen.getValue(),"踢人");
        desc.put(welcome.getValue(),"欢迎");
        desc.put(guiDing.getValue(),"规定");
        desc.put(qieHuan.getValue(),"切换");
        desc.put(tulingKey.getValue(),"图灵机器人Key");
    }

    //构造函数，枚举类型只能为私有
    private RoboSettingContentTypeEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboSettingContentTypeEnum enums) {
        return enums.value;
    }

    public static List<RoboSettingContentTypeEnum> getAllList() {
        RoboSettingContentTypeEnum[] types = RoboSettingContentTypeEnum.values();
        List<RoboSettingContentTypeEnum> result = new ArrayList<RoboSettingContentTypeEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
