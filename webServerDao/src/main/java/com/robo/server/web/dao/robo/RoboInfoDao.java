package com.robo.server.web.dao.robo;

import com.common.base.model.MyPageResult;
import com.robo.server.web.entity.robo.response.RoboInfoResponseDto;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * Created by conor on 2017/8/1.
 */
@Component
public interface RoboInfoDao {

    /**
     * 根据用户Id，查询所有的机器人登录/登出列表
     * @param userId 用户Id
     * @param pageable
     * @return
     * @throws DataAccessException
     */
    public MyPageResult<RoboInfoResponseDto> getRoboInfoPage(final String userId, final Pageable pageable) throws DataAccessException;

}
