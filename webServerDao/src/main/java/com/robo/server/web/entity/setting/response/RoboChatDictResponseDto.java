package com.robo.server.web.entity.setting.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/15.
 */
public class RoboChatDictResponseDto extends BaseDto{

    private String id;
    private String chatKey;//聊天内容
    private String chatValue;//聊天匹配回复内容

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChatKey() {
        return chatKey;
    }

    public void setChatKey(String chatKey) {
        this.chatKey = chatKey;
    }

    public String getChatValue() {
        return chatValue;
    }

    public void setChatValue(String chatValue) {
        this.chatValue = chatValue;
    }
}
