package com.robo.server.web.em.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自动回复是否全量更新 0：是 1：否
 */
public enum RoboChatUpdateFlagEnum {
    YES("0"),
    NO("1");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(YES.getValue(),"是");
        desc.put(NO.getValue(),"否");
    }

    //构造函数，枚举类型只能为私有
    private RoboChatUpdateFlagEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(RoboChatUpdateFlagEnum enums) {
        return enums.value;
    }

    public static List<RoboChatUpdateFlagEnum> getAllList() {
        RoboChatUpdateFlagEnum[] types = RoboChatUpdateFlagEnum.values();
        List<RoboChatUpdateFlagEnum> result = new ArrayList<RoboChatUpdateFlagEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
