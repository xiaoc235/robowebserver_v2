package com.robo.server.web.entity.robo.group;

import com.robo.server.web.entity.BaseTimeDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by conor on 2017/7/12.
 */
@Entity
@Table(name="t_group_chat_content")
public class GroupChatContentDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String chatId;//群组聊天ID
    private String groupId;//群组ID
    private String userId;//用户Id
    private String groupNickName;
    private String personNickName;
    private String content;//聊天内容
    /*
     * 消息类型
     * 1文本消息 3图片消息 34语音消息 37好友确认消息 40POSSIBLEFRIEND_MSG 42共享名片 43视频消息
     * 47动画表情 48位置消息 49分享链接 50VOIPMSG 51微信初始化消息 52VOIPNOTIFY 53VOIPINVITE
     * 62小视频 9999SYSNOTICE 10000系统消息 10002撤回消息
     */
    private String messageType;


    public String getGroupNickName() {
        return groupNickName;
    }

    public void setGroupNickName(String groupNickName) {
        this.groupNickName = groupNickName;
    }

    public String getPersonNickName() {
        return personNickName;
    }

    public void setPersonNickName(String personNickName) {
        this.personNickName = personNickName;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}
