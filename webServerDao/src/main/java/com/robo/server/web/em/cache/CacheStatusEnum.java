package com.robo.server.web.em.cache;


/**
 * Created by jianghaoming on 17/3/7.
 */
public enum CacheStatusEnum {

    PASS("pass_");

    private  String  value ;

    private CacheStatusEnum(final String  _value) {
        this.value = _value;
    }

    public String getValue() {
        return value;
    }
}
