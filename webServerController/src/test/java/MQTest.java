import com.robo.server.mq.EndPoint;
import com.robo.server.web.mq.RabbitMQUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jianghaoming on 17/8/24.
 */
public class MQTest {


    @Test
    public void mq() throws Exception{


        EndPoint.init("127.0.0.1",5672,"robo","robo");

        String endPointName = "topic_robo";
        String clientRoutingKey = "robo.client";

        String roboId = "1234";
        String groupName = "群组1";
        List<String> personList = new ArrayList<>();
        personList.add("张三");

        RabbitMQUtils.tiRenOperate(roboId,groupName,personList);

    }

}
