package com.robo.server.web.model.qrcode;

import com.common.base.BaseRequestModel;
import com.common.base.annotation.CanNullAnnotation;

/**
 * Created by jianghaoming on 17/8/4.
 *
 */
public class QrCodeRequestModel extends BaseRequestModel {

    @CanNullAnnotation
    private String uuid;

    @CanNullAnnotation
    private String robotId;


    public String getRobotId() {
        return robotId;
    }

    public void setRobotId(String robotId) {
        this.robotId = robotId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
