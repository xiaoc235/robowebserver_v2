package com.robo.server.web.controller;

import com.common.base.response.BaseResponseDto;
import com.robo.server.web.base.WebServerController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jianghaoming on 2017/6/13 23:14.
 */

@RestController
@RequestMapping("/test")
public class TestController extends WebServerController{


    @GetMapping("/v1/ping")
    public ResponseEntity<BaseResponseDto> ping(){
      return succResponse("is work!");
    }

}
