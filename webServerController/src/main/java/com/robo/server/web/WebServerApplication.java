package com.robo.server.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import javax.servlet.MultipartConfigElement;


@ComponentScan({"com.robo.server.web.*","com.common.*"})
@EnableAutoConfiguration
@Import(com.robo.server.web.JPAConfig.class)
public class WebServerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WebServerApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(WebServerApplication.class);
	}

	/**
	 * 文件上传大小配置
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize(1024L * 1024L * 10); //设置文件上传大小为10M
		return factory.createMultipartConfig();
	}


}
